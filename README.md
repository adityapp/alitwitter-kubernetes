# Ali Twitter

Twitter-like web app used Rails-Framemwork which can do:

1. Create a tweet
2. Delete a tweet
3. List tweets


## How to Run Application

### Prerequisites

- [Ruby version 2.6.5](https://www.ruby-lang.org/en/documentation/installation/)
- [Bundler 2.1.4](https://bundler.io/)
- [NodeJs](https://nodejs.org/en/download/package-manager/)
- [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)
- [Postgresql](https://www.postgresql.org/download/linux/ubuntu/)


### Install Dependency

Make sure you are in project directory, and follow this command to install all dependency,

```bash
bundle install
```

### Database migration

Modify `config/database.yml` with your postgresql configuration and follow this command to migration

```bash
bundle exec rails db:migrate
```


### Run test

Follow this command to test the application

```bash
bundle exec rails test
```


### Run Server

Follow this command to run application server

```bash
bundle exec rails server
```

Then you can access from browser with `http://localhost:3000/tweets`


## Run Application with Docker

### Prerequisites

- [Docker](https://docs.docker.com/install/)


### Environment Variable

Variable name             | Description                       | Default
------------------------- | --------------------------------- | ------------------------
RAILS_ENV                 | Build Environment                 | development
SECRET_KEY_BASE           | Build secret key base             | 
RAILS_DB_ADAPTER          | Database adapter                  | sqlite3
RAILS_DB_HOST             | Database host                     | 
RAILS_DB_PORT             | Database port                     | 
RAILS_DB_USER             | Database user                     |
RAILS_DB_PASS             | Database password                 | 
RAILS_MAX_THREADS         | Database max threads              | 5
RAILS_DB_DEVELOPMENT_NAME | Database name in development mode | db/development.sqlite3
RAILS_DB_TEST_NAME        | Database name in test mode        | db/test.sqlite3
RAILS_DB_PRODUCTION_NAME  | Database name in production mode  | db/production.sqlite3


### Building the Docker Image

Before build Docker image, you should `db:migrate` first, and to build a Docker Image, following command:

```bash
docker image build -t alitwitter:1.2 .
```


### Spawn a container

To start a Docker container, please follow this command.

```bash
docker container run \
    -e RAILS_ENV=production \
    -e SECRET_KEY_BASE=<put secret key here> \
    --name alitwitter -d \
    -p 3000:3000 \
    alitwitter:1.2
```


## CI/CD

To run with automation please refer to [Automation document](docs/AUTOMATION.md)
