## How to run application with Kubernetes

### Prerequisites

- [VirtualBox](https://www.google.com/search?q=how+to+install+virtualbox&oq=how+to+install+vir&aqs=chrome.0.0l2j69i57j0l5.3625j0j7&sourceid=chrome&ie=UTF-8) 
- [Vagrant](https://www.vagrantup.com/docs/installation)
- [Ansible](https://docs.ansible.com/intro_installation.html#installing-the-control-machine)
- [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/#interacting-with-your-cluster)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Helm 3](https://helm.sh/docs/intro/install/)
- [Bitnami chart](https://github.com/bitnami/charts)


### Start the minikube with virtualbox

To start minikube using virtualbox, please follow this command:
```bash
minikube start --driver=virtualbox
```


### Create and run Runner VM using Vagrant

To create and run runner VM you can use this command:
```bash
vagrant up
```

To remote runner VM you can use this command:
```bash
vagrant ssh
```


### Provisioning for Setup Runner using Ansible

Run this command to setup your runner
```bash
ansible-playbook -i inventory/hosts.yml provisioning/setup_runner.yml
```


### Setup kubectl config
to copy your config, please follow this step:
1. copy `client.crt`, `client.key`, and `ca.crt` value, you can see the PATH use this command:
```bash
kubectl config view
```
2. remote you runner VM using `vagrant ssh`
3. change the user with:
```bash
sudo su gitlab-runner
```
4. create `.kube` directory:
```bash
mkdir ~/.kube/
```
5. create file `client.crt`, `client.key`, and `ca.crt`, and paste the value in each file:
```bash
cd ~/.kube/ #move to '.kube' directory
vim <file name>
```
6. copy `config` value and paste in `~/.kube/` directory:
```bash
cd ~/.kube/ #move to '.kube' directory
vim config
```
7. change certificate path to new PATH, example:
```yml
apiVersion: v1
clusters:
- cluster:
    certificate-authority: ca.crt
    server: https://192.168.99.100:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: client.crt
    client-key: client.key
```
8. set kubectl context:
```bash
kubectl config use-context minikube
```

### Installing postgresql-chart using Helm

To install postgresql-chart, please follow this command:
```bash
helm install postgresql bitnami/postgresql -f values.yaml
```

you can custom [values.yaml](https://github.com/bitnami/charts/blob/master/bitnami/postgresql/values.yaml)


### Installing Alitwitter-chart and Custom value

#### Alitwitter value

Value name                  | Description                       | Default value
--------------------------- | --------------------------------- | ----------------------------------------------------
replicaCount                | Number of node replicas           | 3
image.repository            | alitwitter image repository       | registry.gitlab.com/adityapp/alitwitter-kubernetes
pullPolicy                  | Image pull policy                 | IfNotPresent
nameOverride                | name of chart                     | alitwitter-chart
fullnameOverride            | full name of chart                | alitwitter-chart
service.type                | alitwitter service type           | NodePort
service.port                | alitwitter service port           | 80
database.adapter            | database adapter                  | postgresql
database.user               | database user                     | admin
database.password           | database password                 | verysecret
database.host               | database host                     | postgresql
database.port               | database port                     | 5432
database.developmentName    | database name in development mode | twitter
database.testName           | database name in test mode        | twitter_test
database.productionName     | database name in production mode  | twitter
build.env                   | build mode                        | production
build.secretKeyBase         | build secret key base             | alitwitter_secret_token_here
job.backOffLimit            | job back of limit                 | 5
job.restartPolicy           | job restart policy                | Never
job.command                 | job command                       | ["bundle",  "exec", "rake", "db:migrate"]


#### Install alitwitter-chart

To install alitwitter-chart you can follow this command:
```bash
helm install alitwitter chart/alitwitter
```


### Access the Application
To access the application, you can check url with:
```bash
minikube service alitwitter-chart --url
```

put the url in your browser, and add `/tweets` in end of url
